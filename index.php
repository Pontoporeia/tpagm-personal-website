<?php
  // include('librairies/Parsedown.php');

  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  $file = fopen("info/reading.txt","r");

  $txt_file = file_get_contents('info/reading.txt');
  $rows = explode("\n", $txt_file);
  array_shift($rows);
?>


<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Departure</title>
        <link rel="stylesheet" href="main.css">
        <link rel="icon" type="image/png" href="img/favicon.png">
  </head>
  <body>
	
    <div id="box">
      <h1 id="intro" >?</h1>
      This is my web site.
      <p>The content produced by Théophile Gervreau-Mercier.     School of Art in Mons, Belgium. It varies in shape and size and is mostly experimental work. <br> <br>
      Content can be web based, a 3D piece, paintings and drawings, and if you pop in to look at the gitlab space, there is even a bit of programming.
      <!-- <br> <br>
      On a side note, the school used to have a OVH server on which where hosted FTP access and subdomaine names for students. But as the school switched to a Microsoft hosted Office 365 and Exchange mail servers, they resiliated the ovh server without backing up nor sending word to the students who, might I add, had STUFF in this server...<br><br> -->
      </p>
      <!-- <a id="author" href="tgm.php">--><p id="author">Théophile Gervreau-Mercier</p><!--</a><br><br> -->
      <p class="resize">
      <!-- PS, this window is resizable. Just click on the bottom right corner and drag right. -->
      <br>
      </p>
    </div>
<!-- page to last mention out of social -->


    <div id="social">
      <a href="https://gitlab.com/Zipperflunky"><img class="icon" src="img/gitlab-icon-grey.png" alt=""></a>
      <a href="https://meticulous-manta.itch.io"><img class="icon" src="img/itchio_icon_grey.png" alt=""></a>
    </div>

    <div id="reading">
      <h4>reading_list.txt</h4>
      <?php foreach ($rows as $row => $data) {
              $row_data = explode('-', $data);
              // print_r($row_data);
                if ($row_data[0] == '') {continue;};
              ?>

        <p><em><?= $row_data[0] ?></em> - <mark><?= $row_data[1] ?></mark></p><?php } ?>
    </div>

    <div id="links">
    	  <a class="link" href="tinyblog/index.php"> ~> blog</a><br>
        <a class="link" href="projects/index.php"> ~> projects</a><br>
    </div>
<a id="pagetolast" href="https://jeffhuang.com/designed_to_last/">The page was designed to last</a>
  </body>
</html>
